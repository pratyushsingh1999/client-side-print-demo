import './App.css';
import Example from "./printables/Example";

function App() {
  return (
    <div className="wrapper">
      <Example></Example>
    </div>
  );
}

export default App;
